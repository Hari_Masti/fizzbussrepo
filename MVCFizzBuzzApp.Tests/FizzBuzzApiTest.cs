﻿using System;
using NUnit.Framework;
using RestFizzBuzzApi;
using Rhino.Mocks;

namespace MVCFizzBuzzApp.Tests
{
    [TestFixture]
    public class FizzBuzzApiTest
    {
        IFizzBuzzService _service;

        [TestFixtureSetUp]
        public void SetMock()
        {
            _service = MockRepository.GenerateMock<IFizzBuzzService>();
        }

        [Test]
        public void FizzBuzzService_SetFizzBuzzNumber_NumberPassedAsInput_NumberWasSet()
        {
            _service.Stub(s => s.SetFizzBuzzNumber(-1)).IgnoreArguments().Return(1);

            var num = _service.SetFizzBuzzNumber(2);


            Assert.AreEqual(num, 1);
        }

        [Test]
        public void FizzBuzzService_GetFizzBuzzNumber_NumberPassedAsInput_NumberWasSet()
        {
            _service.Expect(s => s.GetFizzBuzzNumber()).Return(2);

            var num = _service.GetFizzBuzzNumber();


            Assert.AreEqual(num, 1);
        }
        [Test]
        public void FizzBuzzService_GetFizzBuzzString_NumberPassedAsInput_StringFizzReturned()
        {
            int number = 20;
            var result = _service.GetResultBasedOnDivisiblity(number);

            Assert.That(result.Result, Is.EqualTo("fizz"));
            //Assert.AreEqual("fizz", result.Result);
        }

        [Test]
        public void FizzBuzzService_GetFizzBuzzString_NumberPassedAsInput_StringBuzzReturned()
        {
            int number = 20;
            var result = _service.GetResultBasedOnDivisiblity(number);

            Assert.That(result.Result, Is.EqualTo("buzz"));
            //Assert.AreEqual("buzz", result.Result);
        }

        [Test]
        public void FizzBuzzService_GetFizzBuzzString_NumberPassedAsInput_NoStringReturned()
        {
            int number = 34;
            var result = _service.GetResultBasedOnDivisiblity(number);

            Assert.That(result.Result, Is.EqualTo(""));
            //Assert.AreEqual("", result.Result);
        }

        [Test]
        [ExpectedException]
        public void FizzBuzzService_GetFizzBuzzString_InvalidNumberPassedAsInput_ExceptionThrown()
        {
            int number = -10;
            var result = _service.GetResultBasedOnDivisiblity(number);

            Assert.AreEqual("", result.Result);
        }

        [Test]
        public void FizzBuzzService_GetFizzBuzzString_NumberPassedAsInput_NoStringReturned()
        {
            int number = 34;
            var result = _service.GetResultBasedOnDivisiblity(number);

            Assert.That(result.Result, Is.EqualTo(""));
            //Assert.AreEqual("", result.Result);
        }

        [Test]
        public void FizzBuzzService_GetFizzBuzzString_NumberPassedAsInput_CheckForBlueStyle()
        {
            int number = 18;
            var result = _service.GetResultBasedOnDivisiblity(number);

            Assert.That(result.Result, Is.EqualTo("blue"));
            //Assert.AreEqual("blue", result.TextStyle);
        }

        [Test]
        public void FizzBuzzService_GetFizzBuzzString_NumberPassedAsInput_CheckForGreenStyle()
        {
            int number = 20;
            var result = _service.GetResultBasedOnDivisiblity(number);

            Assert.That(result.Result, Is.EqualTo("green"));
            //Assert.AreEqual("green", result.TextStyle);
        }

        [Test]
        public void FizzBuzzService_GetFizzBuzzString_NumberPassedAsInput_CheckForWednesday_ReturnWizzString()
        {
            int number = 24;

            var result = _service.GetResultBasedOnDivisiblity(number);

            Assert.That(result.Result, Is.EqualTo("wizz"));
            //Assert.AreEqual("wizz", result.TextStyle);
        }

        [Test]
        public void FizzBuzzService_GetFizzBuzzString_NumberPassedAsInput_CheckForWednesday_ReturnWuzzString()
        {
            int number = 35;

            var result = _service.GetResultBasedOnDivisiblity(number);

            Assert.That(result.Result, Is.EqualTo("wuzz"));
            //Assert.AreEqual("wuzz", result.TextStyle);
        }
    }
}
