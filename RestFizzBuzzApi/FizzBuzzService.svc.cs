﻿using System.ServiceModel;
using FizzBuzzBusiness.BusinessModel;
using FizzBuzzBusiness.Interfaces;

namespace RestFizzBuzzApi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FizzBuzzService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select FizzBuzzService.svc or FizzBuzzService.svc.cs at the Solution Explorer and start debugging.
    [ServiceContract]
    public class FizzBuzzService : IFizzBuzzService
    {
        private IDivisibility _divisibility;

        public FizzBuzzService(IDivisibility divisibility)
        {
            _divisibility = divisibility;
        }

        internal int FizzBuzzNumber;
        [OperationContract]
        public int SetFizzBuzzNumber(int number)
        {
            FizzBuzzNumber = number;
            return FizzBuzzNumber;
        }

        [OperationContract]
        public int GetFizzBuzzNumber()
        {
            return FizzBuzzNumber;
        }

        [OperationContract]
        public FizzBuzzModel GetResultBasedOnDivisiblity(int number)
        {
            return _divisibility.GetResultBasedOnDivisiblity(number);
        }
    }
}
