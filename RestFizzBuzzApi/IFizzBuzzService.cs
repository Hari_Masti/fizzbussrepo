﻿using FizzBuzzBusiness.BusinessModel;

namespace RestFizzBuzzApi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFizzBuzzService" in both code and config file together.

    public interface IFizzBuzzService
    {

        int SetFizzBuzzNumber(int number);


        int GetFizzBuzzNumber();

        FizzBuzzModel GetResultBasedOnDivisiblity(int number);


    }
}
