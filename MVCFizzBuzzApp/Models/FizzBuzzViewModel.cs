﻿using System.ComponentModel.DataAnnotations;

namespace MVCFizzBuzzApp.Models
{
    public class FizzBuzzViewModel
    {
        [Range(1, 1000, ErrorMessage = "Please enter an integer between 1 to 1000")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Please enter an interger")]
        public int Number { get; set; }

        public string Result { get; set; }

        public string TextStyle { get; set; }

    }
}