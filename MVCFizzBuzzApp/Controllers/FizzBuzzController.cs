﻿using System.Web.Mvc;
using MVCFizzBuzzApp.Models;
using RestFizzBuzzApi;

namespace MVCFizzBuzzApp.Controllers
{
    public class FizzBuzzController : Controller
    {
        private IFizzBuzzService _fizzBuzzService;
        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            _fizzBuzzService = fizzBuzzService;
        }

        public ActionResult FizzBuzzView()
        {
            return View("~/Views/FizzBuzzView.cshtml");
        }

        public JsonResult CheckForDivisibility(int number)
        {
            var model = new FizzBuzzViewModel();
            var resultModel = _fizzBuzzService.GetResultBasedOnDivisiblity(number);
            model.Result = !string.IsNullOrEmpty(resultModel.Result) ? resultModel.Result : string.Empty;
            model.TextStyle = !string.IsNullOrEmpty(resultModel.TextStyle) ? resultModel.TextStyle : string.Empty;
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
