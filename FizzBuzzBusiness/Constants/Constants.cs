﻿namespace FizzBuzzBusiness.Constants
{
    public static class Constants
    {
        public const string Fizz = "fizz";
        public const string Buzz = "buzz";
        public const string Wizz = "wizz";
        public const string Wuzz = "wuzz";

        //Style Constants
        public const string Blue = "Blue";
        public const string Green = "Green";
    }
}
