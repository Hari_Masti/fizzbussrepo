﻿namespace FizzBuzzBusiness.BusinessModel
{
    public class FizzBuzzModel
    {
        public string Result { get; set; }

        public string TextStyle { get; set; }
    }
}
