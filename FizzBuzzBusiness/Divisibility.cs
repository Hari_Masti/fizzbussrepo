﻿using System;
using FizzBuzzBusiness.BusinessModel;

namespace FizzBuzzBusiness
{
    public class Divisibility
    {
        public FizzBuzzModel GetResultBasedOnDivisiblity(int number)
        {
            try
            {
                if (number > 0)
                {
                    var fizzBuzzModel = new FizzBuzzModel();
                    int dayOfTheWeek = (int)DateTime.Now.DayOfWeek;
                    if (number % 3 == 0 && number % 5 == 0)
                    {
                        fizzBuzzModel.Result = Constants.Constants.Fizz + "  " + Constants.Constants.Buzz;
                    }
                    else if (number % 3 == 0)
                    {
                        fizzBuzzModel.Result = dayOfTheWeek == 3 ? Constants.Constants.Wizz : Constants.Constants.Fizz;
                        fizzBuzzModel.TextStyle = Constants.Constants.Blue;
                    }
                    else if (number % 5 == 0)
                    {
                        fizzBuzzModel.Result = dayOfTheWeek == 3 ? Constants.Constants.Wuzz : Constants.Constants.Buzz;
                        fizzBuzzModel.TextStyle = Constants.Constants.Green;
                    }
                    return fizzBuzzModel;
                }
            }
            catch (InvalidOperationException ex)
            {

                throw ex;
            }
            return new FizzBuzzModel();
        }
    }
}
