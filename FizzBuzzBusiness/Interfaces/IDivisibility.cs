﻿using FizzBuzzBusiness.BusinessModel;

namespace FizzBuzzBusiness.Interfaces
{
    public interface IDivisibility
    {
        FizzBuzzModel GetResultBasedOnDivisiblity(int number);
    }
}